import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode


application = Flask(__name__)
app = application

def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    # table_ddl = 'CREATE TABLE message(id INT UNSIGNED NOT NULL AUTO_INCREMENT, greeting TEXT, PRIMARY KEY (id))'
    table_ddl = 'CREATE TABLE movies(id INT UNSIGNED NOT NULL AUTO_INCREMENT, year INT UNSIGNED, title TEXT, director TEXT, actor TEXT, releaseDate TEXT, rating INT UNSIGNED, PRIMARY KEY(id))' 

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        #try:
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        #except Exception as exp1:
        #    print(exp1)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
        populate_data()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)


def populate_data():

    db, username, password, hostname = get_db_creds()

    print("Inside populate_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                       host=hostname,
                                       database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("INSERT INTO message (greeting) values ('Hello, World!')")
    cnx.commit()
    print("Returning from populate_data")


def query_data():

    db, username, password, hostname = get_db_creds()

    print("Inside query_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    cur.execute("SELECT greeting FROM message")
    entries = [dict(greeting=row[0]) for row in cur.fetchall()]
    return entries

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table global")
    create_table()
    print("After create_data global")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None


@app.route('/add_movie', methods=['POST'])
def add_to_db():
    print("Received request.")
    print(request.form['year'])
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    msg = ""
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    if (year == "" or title == "" or director == "" or actor == "" or release_date == "" or rating == ""):
        msg = "Looks like a field is empty. Please correct this."
        return render_template('index.html', message=msg)

    try:
        cur = cnx.cursor()
        cur.execute("SELECT * FROM movies WHERE title = '" + title + "'")
        guys = cur.fetchall()
        if len(guys) == 0:
            cur.execute("INSERT INTO movies (year, title, director, actor, releaseDate, rating) values ('" + year + "', '" + title + "', '" + director + "', '" + actor + "', '" + release_date + "', '" + rating + "')")
            cnx.commit()
            msg = "Success! Movie " + title + " has been added."
        else:
            msg = "Error: Movie " + title + " already exists in the database." 
    except Exception as exp:
        print("error of some kind")
        err = str(exp)
        msg = "Movie could not be inserted:\n" + err
    print(msg)
    return render_template('index.html', message=msg)

@app.route('/update_movie', methods=['POST'])
def update_movie():
    print("Received request.")
    print(request.form['year'])
    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    release_date = request.form['release_date']
    rating = request.form['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    msg = ""
    
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    if (year == "" or title == "" or director == "" or actor == "" or release_date == "" or rating == ""):
        msg = "Looks like a field is empty. Please correct this."
        return render_template('index.html', message=msg)

    try:
        cur = cnx.cursor()
        cur.execute("SELECT * FROM movies WHERE title = '" + title + "'")
        guys = cur.fetchall()
        if len(guys) == 0:
            msg = "Error: Movie " + title + " does not currently exist in the database."
        else:
            cur.execute("UPDATE movies\n SET year = '" + year + "', title = '" + title + "', director = '" + director + "', actor = '" + actor + "', releaseDate = '" + release_date + "', rating = '" + rating + "'\n WHERE title = '" + title + "'")
            cnx.commit()
            msg = "Success! Movie " + title + " has been updated."
    except Exception as exp:
        print("error of some kind")
        err = str(exp)
        msg = "Movie could not be inserted:\n" + err
    return render_template('index.html', message=msg)

@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    title = request.form['delete_title']

    db, username, password, hostname = get_db_creds()
    cnx = ''
    msg = ""
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    if title == "":
        return render_template('index.html', message="Please enter a valid title.")

    try:
        cur = cnx.cursor()
        cur.execute("SELECT * FROM movies WHERE title = '" + title + "'")
        guys = cur.fetchall()
        if len(guys) == 0:
            msg = "Error: Movie " + title + " does not currently exist in the database."
        else:
            cur.execute("DELETE FROM movies WHERE title = '" + title +"'" )
            cnx.commit()
            msg = "Success! Movie " + title + " has been deleted."
    except Exception as exp:
        print("error of some kind")
        err = str(exp)
        msg = err
    return render_template('index.html', message=msg)

@app.route('/search_movie', methods=['GET'])
def search_movie():
    db, username, password, hostname = get_db_creds()

    cnx = ''
    msg = ""
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    actor = request.args.get('search_actor')
    if actor == "":
        return render_template('index.html', message="Actor name in search should not be empty.")

    cur = cnx.cursor()
    cur.execute("SELECT title, year, actor FROM movies WHERE actor = '" + actor  + "'")
    results = cur.fetchall()
    movies_list = []
    if len(results) == 0:
        return render_template('index.html', message = "No movies found for actor " + actor + ".")
    else:
        for r in results:
            movie = {'title': r[0], 'year': str(r[1]), 'actor': r[2]}
            movies_list.append(movie)
    cnx.commit()
    return render_template('index.html', message = msg, movies = movies_list)

@app.route('/highest_rating', methods=['GET'])
def highest_rating():
    db, username, password, hostname = get_db_creds()

    cnx = ''
    msg = ""
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    ratings_list = []
    cur = cnx.cursor()
    cur.execute("SELECT title, year, actor, director, rating FROM movies WHERE rating=(SELECT MAX(rating) FROM movies)")
    results = cur.fetchall()
    if len(results) == 0:
        return render_template('index.html', message = "No movies in the database.")
    else:
        for r in results:
            rating = {'title': r[0], 'year': str(r[1]), 'actor': r[2], 'director': r[3], 'rating': str(r[4])}
            ratings_list.append(rating)
    cnx.commit()
    return render_template('index.html', message = msg, ratings = ratings_list)

@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
    db, username, password, hostname = get_db_creds()

    cnx = ''
    msg = ""
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    ratings_list = []
    cur = cnx.cursor()
    cur.execute("SELECT title, year, actor, director, rating FROM movies WHERE rating=(SELECT MIN(rating) FROM movies)")
    results = cur.fetchall()
    if len(results) == 0:
        return render_template('index.html', message = "No movies in the database.")
    else:
        for r in results:
            rating = {'title': r[0], 'year': str(r[1]), 'actor': r[2], 'director': r[3], 'rating': str(r[4])}
            ratings_list.append(rating)
    cnx.commit()
    return render_template('index.html', message = msg, ratings = ratings_list)

@app.route("/")
def hello():
    print("Inside hello")
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    entries = query_data()
    print("Entries: %s" % entries)
    return render_template('index.html', entries=entries)


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
